#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "library.h"

int main()
{
item_t t;
char name[30];
int choice;
do{
printf("\n0. Exit \n1. add item \n2. find item \n3. display all item \n4. edit file \n5.delete file \nEnter choice: ");
scanf("%d",&choice);

switch(choice)
{
    case 1: // add item

        add_item();
    break;
    case 2: // find item
        printf("enter name of the item :");
        scanf("%s",name);
            find_item_by_name(name);
            
    case 3: // display all items
                display_all();
    break;
    case 4: // edit file
            edit_file();
    break;
    case 5: // delete file
        delete_by_id();
    break;

}


}
while(choice != 0);
}
// switch case

//add 
void add_item(){
     FILE *fp;
 //open item file
   item_t t;
   accept_item(&t);
   t.id = get_next_item_id();
  fp = fopen(ITEM_DB, "ab+");
  if(fp ==NULL){
      perror("item_file not found");
        exit(1);
  
  } 
        //append item
        fwrite(&t, sizeof(item_t),1, fp);
        printf("file added into the file.");
      display_item(&t);
        fclose(fp);
    


 }
//id generated automatically
  int get_next_item_id(){

    FILE *fp;
    int max = 0;
    int size = sizeof(item_t);
    item_t t;
    //open the file
    fp = fopen(ITEM_DB, "rb");
    if(fp ==NULL)
    return max +1;
    // change file pos to the last
    fseek(fp, -size, SEEK_END);
    //read record
    if(fread(&t, size, 1,fp)>0)

    max= t.id;
    fclose(fp);
    return max +1;
}


//accept item
void accept_item(item_t *t){
t->id = get_next_item_id();
//printf("Id:");
//scanf("%d",t->id);
printf("NAME:");
scanf("%s", t->name);
printf("Price:");
scanf("%f",t->price);                  
printf("quantity:");
scanf("%d",t->quantity);


}

//display item



 void display_item(item_t *t){

   
 printf("%d, %s, %.2f, %d\n",t->id, t->name, t->price[0], t->quantity[0]);
 }


 
// find
//display all
/*
void display(){


item_t t;
    display_item(&t);
}
*/
//find item by name
void find_item_by_name(char name[]){
  FILE *fp;
        int found=0;
        item_t t;

        //open the file
          fp =  fopen(ITEM_DB, "rb");
          if(fp==NULL){
              perror("Books are not avalaible");
              return;
          }
        //read one books 1 by 1
        while(fread(&t, sizeof(item_t), 1, fp)>0){
                //if book name is matching partially, found 1

        if(strstr(t.name,name) !=NULL){
            found =1;
            display_item(&t);
            }

        
        }
                //close file
                fclose(fp);
                if(!found)
                    printf("No such book found \n");

}



//edit
void edit_file(){
    int id, found =0;
    FILE *fp;
        item_t t;
        //take id from user
        printf("Enter id of the item you want to edit");
        scanf("%d",&id);
      // open file
      fp = fopen(ITEM_DB,"rb+");

      if(fp ==NULL){
          perror("File not found");
          exit(1);
      }
        while (fread(&t, sizeof(item_t),1,fp)>0)
        {
            /* code */
            if(id == t.id)
            found = 1;
            break;
        }
        //if found()
        if(found){

           long size =sizeof(item_t);
           item_t m;
           accept_item(&m);
           t.id = m.id;
           //take file position from ane record behind
           fseek(fp,-size, SEEK_CUR);
           //overwrite
           fwrite(&m, size, 1, fp);
           printf("No such book found \n");
        }
        else{
            printf("NO such item exist");
        }
        fclose(fp);

    
}

void display_all(){
     int count =0 ;
        FILE *fp;
        item_t t;

       

        //open thebook copies file
          fp =  fopen(ITEM_DB, "rb");
          if(fp==NULL){
              perror("Cannot open bookcopies file.");
              return;
          }
        //read one bookcopy record 1 by 1
        while(fread(&t, sizeof(item_t),1,fp)>0){
    
            {
                   display_item(&t);
                    count++;
            }
        }
                fclose(fp);

                if(count == 0){
                printf("no items availables.\n");    
                } 
}

int delete_by_id(){

     item_t t;
     FILE *fp, *ft;

     int n;

     fp = fopen(ITEM_DB,"r");
     ft = fopen(TEMP_DB,"wb+");


     printf("\n Enter name of item to delete : ");

     
     scanf("%s", &n);
     rewind(fp);
     while(!feof(fp)){




         fscanf(fp, "%d %s %f %d", &t.id, &t.name, &t.price, &t.quantity);
         if(t.id != n){
             fprintf(ft, "%d %s %f %d\n", t.id, t.name, t.price, t.quantity);
         }

     } 
     

     rename(TEMP_DB,ITEM_DB);
         remove(ITEM_DB);   

         
     fclose(fp);

     fclose(ft);

     
     return 0;
}
