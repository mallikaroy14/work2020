#include<stdio.h>
#include<stdlib.h>
#include<string.h>


// file of item
#define   ITEM_DB "item.db"
#define TEMP_DB  "Temp.db"
typedef struct item
{
    int id;
    char name[40];
    float price[10];
    int quantity[10];
    /* data */
}item_t;

 void add_item();
 void accept_item(item_t *t);
 void display_item(item_t *t);
 int get_next_item_id();
 void find_item_by_name(char name[]);
 void display_all();
 void edit_file();
 int delete_by_id();